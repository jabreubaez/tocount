//
//  TrafficCountControllerViewController.m
//  ToCount
//
//  Created by Roberto Abreu on 16/01/14.
//  Copyright (c) 2014 Roberto Abreu. All rights reserved.
//

#import "TrafficCountViewController.h"

@interface TrafficCountViewController ()


@end

@implementation TrafficCountViewController

@synthesize lblFecha, pickerFecha;


- (void)viewDidLoad
{
    [super viewDidLoad];
	 [self.pickerFecha addTarget:self action:@selector(cambioFecha) forControlEvents:UIControlEventValueChanged];
    
    self.lblFecha.textColor = [UIColor colorWithRed:233.0/255.0 green:191.0/255.0 blue:36.0/255.0 alpha:1];
    self.txtAddress.textColor = [UIColor colorWithRed:233.0/255.0 green:191.0/255.0 blue:36.0/255.0 alpha:1];
    
    //Bordes
    UIView *bordeTopPri = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.contenedorPri.frame.size.width, 1)];
    bordeTopPri.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:243.0/255.0 blue:243.0/255 alpha:1];

    
    UIView *bordeBottomPri = [[UIView alloc] initWithFrame:CGRectMake(0, self.contenedorPri.frame.size.height-2, self.contenedorPri.frame.size.width, 1)];
    bordeBottomPri.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:243.0/255.0 blue:243.0/255 alpha:1];
    
    UIView *bordeTopSec = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.contenedorPri.frame.size.width, 1)];
    bordeTopSec.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:243.0/255.0 blue:243.0/255 alpha:1];
    
    
    UIView *bordeBottomSec = [[UIView alloc] initWithFrame:CGRectMake(0, self.contenedorSec.frame.size.height-2, self.contenedorPri.frame.size.width, 1)];
    bordeBottomSec.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:243.0/255.0 blue:243.0/255 alpha:1];
    
    [self.contenedorPri addSubview:bordeTopPri];
    [self.contenedorPri addSubview:bordeBottomPri];
    
    [self.contenedorSec addSubview:bordeTopSec];
    [self.contenedorSec addSubview:bordeBottomSec];
    
    [self performSelector:@selector(cambioFecha)];
    [self inicializarLocalizador];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*Metodos definidos por mi*/

-(void) cambioFecha{
    NSDate *fecha = self.pickerFecha.date;
    NSDateFormatter *formato = [[NSDateFormatter alloc] init];
    [formato setDateFormat:@"MMM dd, yyyy h:mm a"];
    self.lblFecha.text = [formato stringFromDate:fecha];
    NSLog(@"%@",[formato stringFromDate:fecha]);
}

-(void) inicializarLocalizador{
    if([CLLocationManager locationServicesEnabled]){
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        NSLog(@"Here");
        [self.locationManager startUpdatingLocation];
    }else{
        NSLog(@"There");
        [[[UIAlertView alloc] initWithTitle:@"Alerta" message:@"Verifique si tienes habilitado los servicios de GPS" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil]show];
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    [self.btnAddress setHidden:YES];
    [self.activityIndicator startAnimating];
    CLLocation * location = [locations lastObject];
    NSLog(@"%@",[NSString stringWithFormat:@"%f %f",location.coordinate.latitude,location.coordinate.longitude]);
    CLGeocoder *codificador = [[CLGeocoder alloc] init];
    [codificador reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        if([placemarks count] > 0 && error == nil){
            CLPlacemark *marca = placemarks[0];
            NSLog(@"Ciudad : %@",marca.locality);
            NSLog(@"Postal Code : %@",marca.postalCode);
            NSLog(@"Ciudad : %@",marca.country);
            NSLog(@"Provincia : %@",marca.administrativeArea);
            NSLog(@"Calle : %@",marca.thoroughfare);
            NSLog(@"Numero calle : %@",marca.subThoroughfare);
            
            self.txtAddress.text = [NSString stringWithFormat:@"%@, %@, %@, %@, %@",marca.country,marca.administrativeArea,marca.locality,marca.thoroughfare,marca.subThoroughfare];
            
            [self.locationManager stopUpdatingLocation];
                   }else if([placemarks count] == 0){
             [[[UIAlertView alloc] initWithTitle:@"Alerta" message:@"Su ubicacion no ha podido ser detectada" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil]show];
        }else if(error){
             [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Ha ocurrido un error al tratar de obtener su ubicacion" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil]show];
        }
        [self.activityIndicator stopAnimating];
        [self.btnAddress setHidden:NO];
    }];
    
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    if([self.txtAddress isFirstResponder] && [touch view]!=self.txtAddress)
        [self.txtAddress resignFirstResponder];
    
    [super touchesBegan:touches withEvent:event];
}

- (IBAction)getAddress:(id)sender{
    [self inicializarLocalizador];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"InicioConteo"]){
        CountViewController * countViewController = [segue destinationViewController];
        countViewController.address = self.txtAddress.text;
    }
}

@end
