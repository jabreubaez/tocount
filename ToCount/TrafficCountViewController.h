//
//  TrafficCountControllerViewController.h
//  ToCount
//
//  Created by Roberto Abreu on 16/01/14.
//  Copyright (c) 2014 Roberto Abreu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "CountViewController.h"

@interface TrafficCountViewController : UIViewController <CLLocationManagerDelegate>
@property (strong, nonatomic) IBOutlet UIButton *btnAddress;

@property (nonatomic,strong) IBOutlet UILabel * lblFecha;
@property (strong, nonatomic) IBOutlet UIDatePicker *pickerFecha;
@property (strong,nonatomic) CLLocationManager *locationManager;
@property (strong,nonatomic) IBOutlet UITextField *txtAddress;
@property (strong,nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong,nonatomic) IBOutlet UIView * contenedorPri;
@property (strong,nonatomic) IBOutlet UIView * contenedorSec;


- (IBAction)getAddress:(id)sender;

@end
