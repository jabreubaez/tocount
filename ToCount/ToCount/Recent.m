//
//  Recent.m
//  ToCount
//
//  Created by Roberto Abreu on 19/01/14.
//  Copyright (c) 2014 Roberto Abreu. All rights reserved.
//

#import "Recent.h"

@implementation Recent

@synthesize address,cantidad;

-(id)initWithCount:(NSString*)count andAddress:(NSString*)dir{
    
    self = [super init];
    if(self){
        cantidad = count;
        address = dir;
    }
    return self;
}

@end
