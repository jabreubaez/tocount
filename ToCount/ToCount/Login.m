//
//  Login.m
//  ToCount
//
//  Created by Roberto Abreu on 22/01/14.
//  Copyright (c) 2014 Roberto Abreu. All rights reserved.
//

#import "Login.h"

@implementation Login

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
   
    CGContextRef ref = UIGraphicsGetCurrentContext();
    
    UIImage * img = [UIImage imageNamed:@"bg.jpg"];
    [img drawInRect:rect];
    
    UIColor * colorBlanco = [[UIColor alloc]initWithRed:1 green:1 blue:1 alpha:.4];
    
    CGContextSetFillColorWithColor(ref, colorBlanco.CGColor);
    
    CGContextFillRect(ref, CGRectMake(63, 258, 228, 35));
    
    CGContextFillRect(ref, CGRectMake(63, 300, 228, 35));
    
    CGContextSetStrokeColorWithColor(ref, [UIColor whiteColor].CGColor);
   /* CGContextBeginPath(ref);
    CGContextMoveToPoint(ref, 0, 520);
    CGContextAddLineToPoint(ref, 320, 520);
    CGContextClosePath(ref);
    CGContextSetLineWidth(ref,.1);
    CGContextStrokePath(ref);*/
    
    //CGContextDrawImage(ref, CGRectMake(0,0,320,580),img.CGImage);
    
}


@end
