//
//  UIImage+Tint.h
//  ToCount
//
//  Created by Roberto Abreu on 18/01/14.
//  Copyright (c) 2014 Roberto Abreu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Tint)

- (UIImage *)tintedGradientImageWithColor:(UIColor *)tintColor;
- (UIImage *)tintedImageWithColor:(UIColor *)tintColor;
- (UIImage *)tintedWithLinearGradientColors:(NSArray *)colorsArr;
- (UIImage *)imageWithGaussianBlur;
- (UIImage *)tintedGradientImageWithColor:(UIColor *)tintColor andButton:(UIButton*)btn;
@end
