//
//  Recent.h
//  ToCount
//
//  Created by Roberto Abreu on 19/01/14.
//  Copyright (c) 2014 Roberto Abreu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Recent : NSObject

@property (nonatomic,strong) NSString * cantidad;
@property (nonatomic,strong) NSString * address;

-(id)initWithCount:(NSString*)count andAddress:(NSString*)dir;

@end
