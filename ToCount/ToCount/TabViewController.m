//
//  TabViewController.m
//  ToCount
//
//  Created by Roberto Abreu on 18/01/14.
//  Copyright (c) 2014 Roberto Abreu. All rights reserved.
//

#import "TabViewController.h"

@interface TabViewController ()

@end

@implementation TabViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    for (UITabBarItem *item in [[self tabBar] items]) {
        [item setSelectedImage:[self retornarImagen:[item tag] selecion:NO]];
        [item setSelectedImage:[self retornarImagen:[item tag] selecion:YES]];
    }
    
}

-(UIImage*) retornarImagen:(NSInteger)pos selecion:(BOOL)sele{
    NSString * nombre;
    
    switch (pos) {
        case 0:
            nombre = [NSString stringWithFormat:@"recents%@_icon.png",(!sele)?@"":@"_fill"];
            break;
        case 1:
            nombre = [NSString stringWithFormat:@"add%@_icon.png",(!sele)?@"":@"_fill"];
            break;
        case 2:
            nombre = [NSString stringWithFormat:@"share%@_icon.png",(!sele)?@"":@"_fill"];
            break;
        case 3:
            nombre = [NSString stringWithFormat:@"settings%@_icon.png",(!sele)?@"":@"_fill"];
            break;
        default:
            break;
    }
    
    return [UIImage imageNamed:nombre];
}
    
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
