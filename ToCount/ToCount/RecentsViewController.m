//
//  RecentsViewController.m
//  ToCount
//
//  Created by Roberto Abreu on 19/01/14.
//  Copyright (c) 2014 Roberto Abreu. All rights reserved.
//

#import "RecentsViewController.h"
#import "RecentsCell.h"

@interface RecentsViewController ()

@end

@implementation RecentsViewController

@synthesize listaRecents, buscador, filtroListaRecents;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)mostrarBuscador:(id)sender {
    [UIView animateWithDuration:.5 animations:
     ^{
         [self.tableView setContentOffset:CGPointMake(0, -self.buscador.frame.size.height - 20) animated:NO];
     } completion:^(BOOL finished) {
         [self.buscador becomeFirstResponder];
     }];
}
- (IBAction)mostrarSort:(id)sender {
  UIActionSheet *sortOptions = [[UIActionSheet alloc]initWithTitle:@"Sort By" delegate:nil cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Address",@"Date",@"Low Quantity",@"High Quantity", nil];
    
   
    
    [sortOptions showInView:self.view];
    for(UIView *v in sortOptions.subviews){
        if([v isKindOfClass:[UIButton class]]){
            UIButton * btnTemp = (UIButton*) v;
            [btnTemp setTitleColor:[UIColor colorWithRed:233.0/255.0 green:191.0/255.0 blue:36.0/255.0 alpha:1] forState:UIControlStateNormal];
        }
    }
}


-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
           CGRect bounds = self.tableView.bounds;
        bounds.origin.y = bounds.origin.y + self.buscador.frame.size.height;
        self.tableView.bounds = bounds;

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Recent *reciente1 = [[Recent alloc] initWithCount:@"2,100 Vehicles" andAddress:@"Today, on Lopez de Vega Street"];
    
    Recent *reciente2 = [[Recent alloc] initWithCount:@"320 Vehicles" andAddress:@"Today, on Cars Monsuel Sarmin Elex."];
    
    Recent *reciente3 = [[Recent alloc] initWithCount:@"1,500 Vehicles" andAddress:@"Today, on Carnige Baberun Colonial Street"];
    
    Recent *reciente4 = [[Recent alloc] initWithCount:@"670 Vehicles" andAddress:@"Today, on Sabana Perdida Street"];
    
    
    listaRecents = [[NSMutableArray alloc] initWithObjects:reciente1,reciente2,reciente3,reciente4,reciente1,reciente2,reciente3,reciente4, nil];
    
    self.filtroListaRecents = [[NSMutableArray alloc] initWithCapacity:[listaRecents count]];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    CGRect bounds = self.tableView.bounds;
    bounds.origin.y = bounds.origin.y + buscador.frame.size.height;
    self.tableView.bounds = bounds;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == self.searchDisplayController.searchResultsTableView){
        return [filtroListaRecents count];
    }else{
        return [listaRecents count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"recents_cell";
    RecentsCell *cell = (RecentsCell*)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Recent * recienteTemp;
    
    if(tableView == self.searchDisplayController.searchResultsTableView){
        recienteTemp = [filtroListaRecents objectAtIndex:indexPath.row];
    }else{
        recienteTemp = [listaRecents objectAtIndex:indexPath.row];
    }

    cell.lblCount.text = recienteTemp.cantidad;
    cell.lblAddress.text = recienteTemp.address;
    
    return cell;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [listaRecents removeObjectAtIndex:indexPath.row];
       
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


-(void) filtrarContenido:(NSString*)textoBusqueda{
    [self.filtroListaRecents removeAllObjects];
    
    NSPredicate * filtro = [NSPredicate predicateWithFormat:@"SELF.address contains[c] %@ or SELF.cantidad contains[c] %@",textoBusqueda,textoBusqueda];
    
    filtroListaRecents = [NSMutableArray arrayWithArray:[listaRecents filteredArrayUsingPredicate:filtro]];
    
    NSLog(@"Cantidad Filtrada : %i", [filtroListaRecents count]);
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString{
    
    [self filtrarContenido:searchString];
    
    return YES;
}

-(void) searchDisplayController:(UISearchDisplayController *)controller willShowSearchResultsTableView:(UITableView *)tableView{
    
    [tableView setRowHeight:[self.tableView rowHeight]];
    [tableView reloadData];
    
}



@end
