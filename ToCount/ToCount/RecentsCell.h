//
//  RecentsCell.h
//  ToCount
//
//  Created by Roberto Abreu on 19/01/14.
//  Copyright (c) 2014 Roberto Abreu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecentsCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblCount;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;

@end
