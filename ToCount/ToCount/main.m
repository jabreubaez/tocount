//
//  main.m
//  ToCount
//
//  Created by Roberto Abreu on 16/01/14.
//  Copyright (c) 2014 Roberto Abreu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
