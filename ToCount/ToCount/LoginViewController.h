//
//  LoginViewController.h
//  ToCount
//
//  Created by Roberto Abreu on 22/01/14.
//  Copyright (c) 2014 Roberto Abreu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController

@property (nonatomic,strong) IBOutlet UIButton * btnLogin;

@end
