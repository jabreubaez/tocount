//
//  CountViewController.m
//  ToCount
//
//  Created by Roberto Abreu on 16/01/14.
//  Copyright (c) 2014 Roberto Abreu. All rights reserved.
//

#import "CountViewController.h"
#import "UIImage+Tint.h"

@interface CountViewController ()

@end

@implementation CountViewController{
    int horas, minutos, segundos;
    NSTimer *timer;
}

@synthesize lblAddress, address, listaCategoriaConteo, lblCronometro, listaRedo, listaUndo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.jpg"]];
    self.lblAddress.text = address;
    
    self.listaCategoriaConteo = [NSMutableArray arrayWithObjects:@0,@0,@0,@0,@0,@0,nil];
    [self iniciarContadores];
    
    self.listaRedo = [[NSMutableArray alloc] init];
    self.listaUndo = [[NSMutableArray alloc] init];
    
    
    
    [self.redo setEnabled:NO];
    [self.undo setEnabled:NO];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(mostrarCronometro) userInfo:nil repeats:YES];
}

-(void) mostrarCronometro{
    segundos++;
    if(segundos == 60){
        segundos=0;
        minutos++;
        if(minutos == 60)
        {
            minutos = 0;
            horas++;
        }
    }
    [lblCronometro setText:[NSString stringWithFormat:@"%02i:%02i:%02i",horas,minutos,segundos]];
    
    //Para comprobar si han realizado algun conteo
    if([listaUndo count]>0){
        [self.undo setEnabled:YES];
    }else{
        [self.undo setEnabled:NO];
    }
    
    if([listaRedo count]>0){
        [self.redo setEnabled:YES];
    }else{
        [self.redo setEnabled:NO];
    }
}

- (IBAction)contar:(UIButton*)sender {
    int tagButton = [sender tag] - 1;
    
    int cantidad = [(NSNumber*)self.listaCategoriaConteo[tagButton] intValue] + 1;
    
    self.listaCategoriaConteo[tagButton] = [NSNumber numberWithInt:cantidad];
    
    [sender setTitle:[NSString stringWithFormat:@"%i",cantidad] forState:UIControlStateNormal];
    
    //agregar a lista de Undo
    [listaUndo addObject:[NSNumber numberWithInt:[sender tag]]];
    NSLog(@"Tam %f %f",self.undo.frame.size.width,self.undo.frame.size.height);
    
}

- (IBAction)undo:(id)sender {
     if([listaUndo count] > 0){
    NSNumber *valor = [listaUndo lastObject];
    int tagButton = [valor intValue];
    [listaRedo addObject:valor];
    [listaUndo removeObjectAtIndex:[listaUndo count]-1];
    int cantidad = [(NSNumber*)self.listaCategoriaConteo[tagButton-1] intValue] - 1;
    self.listaCategoriaConteo[tagButton-1] = [NSNumber numberWithInt:cantidad];
    UIButton * btnTemp = (UIButton*)[self.view viewWithTag:tagButton];
    [btnTemp setTitle:[NSString stringWithFormat:@"%i",cantidad] forState:UIControlStateNormal];
    }
}

- (IBAction)guardarConteo:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (IBAction)redo:(id)sender {
    if([listaRedo count] > 0){
    NSNumber *valor = [listaRedo lastObject];
    int tagButton = [valor intValue];
    [listaUndo addObject:valor];
    [listaRedo removeObjectAtIndex:[listaRedo count]-1];
    int cantidad = [(NSNumber*)self.listaCategoriaConteo[tagButton-1] intValue] + 1;
    self.listaCategoriaConteo[tagButton-1] = [NSNumber numberWithInt:cantidad];
    UIButton * btnTemp = (UIButton*)[self.view viewWithTag:tagButton];
    [btnTemp setTitle:[NSString stringWithFormat:@"%i",cantidad] forState:UIControlStateNormal];
    }
}

- (void)iniciarContadores{
    for(int i=1;i<=6;i++){
        UIButton *btnTemp = (UIButton *)[self.view viewWithTag:i];
        btnTemp.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
        btnTemp.titleLabel.textAlignment = NSTextAlignmentCenter;
        [btnTemp setTitle:[NSString stringWithFormat:@"0"] forState:UIControlStateNormal];
        
        
       // [btnTemp setBackgroundImage:[[UIImage imageNamed:@"circle_icon.png"] tintedGradientImageWithColor:[UIColor colorWithRed:(233.0-i*10)/255.0 green:(191.0-i*2)/255.0 blue:(36.0-i*3)/255.0 alpha:1] andButton:btnTemp] forState:UIControlStateNormal];
        
      
      //  NSArray *listaColores = [NSArray arrayWithObjects:[UIColor whiteColor],[UIColor colorWithRed:233.0/255.0 green:191.0/255.0 blue:36.0/255.0 alpha:1], nil];
        
       //  [btnTemp setBackgroundImage:[[UIImage imageNamed:@"circle_icon.png"] tintedWithLinearGradientColors:listaColores] forState:UIControlStateNormal];
    
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
