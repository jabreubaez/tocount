//
//  RecentsViewController.h
//  ToCount
//
//  Created by Roberto Abreu on 19/01/14.
//  Copyright (c) 2014 Roberto Abreu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Recent.h"

@interface RecentsViewController : UITableViewController <UISearchBarDelegate,UISearchDisplayDelegate,UIActionSheetDelegate>

@property (nonatomic,strong) NSMutableArray * listaRecents;

@property (nonatomic,strong) NSMutableArray * filtroListaRecents;
@property (nonatomic,strong) IBOutlet UISearchBar * buscador;

@property (nonatomic,strong) IBOutlet UIBarButtonItem * btnSort;

@end
