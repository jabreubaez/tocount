//
//  Conteo.m
//  ToCount
//
//  Created by Roberto Abreu on 18/01/14.
//  Copyright (c) 2014 Roberto Abreu. All rights reserved.
//

#import "Conteo.h"

@implementation Conteo

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    // Drawing code
    CGContextRef ref = UIGraphicsGetCurrentContext();
    
    CGImageRef refI = [[UIImage imageNamed:@"background.jpg"] CGImage];
   
    CGContextDrawImage(ref, CGRectMake(0, 0, 320, 580), refI);
    
    CGContextSetBlendMode(ref, kCGBlendModeOverlay);

    CGContextSetStrokeColorWithColor(ref, [UIColor whiteColor].CGColor);

    CGContextStrokeEllipseInRect(ref, CGRectMake(20, 198, 83, 83));
    CGContextStrokeEllipseInRect(ref, CGRectMake(119, 198, 83, 83));
    CGContextStrokeEllipseInRect(ref, CGRectMake(217, 198, 83, 83));
    CGContextStrokeEllipseInRect(ref, CGRectMake(20, 322, 83, 83));
    CGContextStrokeEllipseInRect(ref, CGRectMake(119, 322, 83, 83));
    CGContextStrokeEllipseInRect(ref, CGRectMake(217, 322, 83, 83));

}


@end
