//
//  CountViewController.h
//  ToCount
//
//  Created by Roberto Abreu on 16/01/14.
//  Copyright (c) 2014 Roberto Abreu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountViewController : UIViewController

@property(strong,nonatomic) IBOutlet UILabel * lblAddress;
@property(strong,nonatomic) NSString * address;

@property(strong,nonatomic) IBOutlet UILabel * lblCronometro;

@property(strong,nonatomic) NSMutableArray * listaCategoriaConteo;
@property(strong,nonatomic) NSMutableArray * listaUndo;
@property(strong,nonatomic) NSMutableArray * listaRedo;

- (IBAction)contar:(id)sender;
- (IBAction)undo:(id)sender;
- (IBAction)guardarConteo:(id)sender;
- (IBAction)redo:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *undo;
@property (strong, nonatomic) IBOutlet UIButton *redo;

@end
